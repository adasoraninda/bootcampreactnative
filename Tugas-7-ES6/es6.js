// Soal ES6

// Soal 1 (Funsi Arrow)
console.log("Soal 1 (Fungsi Arrow)");
const golden = () => {
  console.log("this is golden!!");
};
golden();

// Soal 2 (Sederhanakan Object)
console.log("\nSoal 2 (Sederhanakan Object)");
const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName: () => console.log(firstName + " " + lastName),
  };
};
//Driver Code
newFunction("William", "Imoh").fullName();

// Soal 3 (Destructing)
console.log("\nSoal 3 (Destructing)");
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};
const { firstName, lastName, destination, occupation } = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);

// Soal 4 (Array Spreading)
console.log("\nSoal 4 (Array Spreading)");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

// Soal 5 (Template Literals)
console.log("\nSoal 5 (Template Literals)");
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet,  
    consectetur adipiscing elit, ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`;
// Driver Code
console.log(before);
