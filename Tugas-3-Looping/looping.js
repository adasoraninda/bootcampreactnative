// Tugas Looping

// Soal 1 Looping while
console.log("Soal 1 Looping while");
var i = 0;
var endIndex = 10;
var increment = 2;
var isLoop1 = true;

while (i <= endIndex) {
  if (isLoop1) {
    if (i == 0) {
      console.log("LOOPING PERTAMA");
    } else {
      console.log(`${i * increment} - I love coding`);
    }
  }
  if (!isLoop1) {
    if (i == 0) {
      console.log("LOOPING KEDUA");
    } else {
      console.log(
        `${
          endIndex * increment - i * increment + increment
        } - I will become a mobile developer`
      );
    }
  }

  i++;

  if (i > endIndex) {
    if (isLoop1) {
      isLoop1 = false;
      i = 0;
    }
  }
}

// Soal 2 Looping for
console.log("\nSoal 2 Looping for");
for (i = 1; i <= 20; i++) {
  if (i % 3 == 0 && i % 2 == 1) {
    console.log(`${i} - I Love Coding`);
  } else if (i % 2 == 1) {
    console.log(`${i} - Santai`);
  } else {
    console.log(`${i} - Berkualitas`);
  }
}

// Soal 3 Membuat Persegi Panjang #
console.log("\nSoal 3 Membuat Persegi Panjang #");
for (i = 0; i < 4; i++) {
  var board = "";
  for (j = 0; j < 8; j++) {
    board += "#";
  }
  console.log(board);
}

// Soal 4 Membuat Tangga
console.log("\nSoal 4 Membuat Tangga");
for (i = 0; i < 7; i++) {
  var board = "";
  for (j = 0; j <= i; j++) {
    board += "#";
  }
  console.log(board);
}

// Soal 5 Membuat Papan Catur
console.log("\nSoal 5 Membuat Papan Catur");
for (i = 0; i < 8; i++) {
  var board = "";
  for (j = 0; j < 8; j++) {
    if (i % 2 == 0) {
      if (j % 2 == 0) {
        board += " ";
      } else {
        board += "#";
      }
    } else {
      if (j % 2 == 0) {
        board += "#";
      } else {
        board += " ";
      }
    }
  }
  console.log(board);
}
