// Soal Class

// Soal 1 (Animal Class) Release 0
class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
}

// Soal 1 (Animal Class) Release 1
class Frog extends Animal {
  constructor(name) {
    super(name);
  }

  jump() {
    console.log("hop hop");
  }
}

class Ape extends Animal {
  constructor(name) {
    super(name);
    this.legs = 2;
  }

  yell() {
    console.log("Auooo");
  }
}

// Soal 2 (Function to Class)
class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

function main() {
  // Soal 1 (Animal Class) Release 0
  console.log("Soal 1 Release 0");
  var sheep = new Animal("shaun");

  console.log(sheep.name); // "shaun"
  console.log(sheep.legs); // 4
  console.log(sheep.cold_blooded); // false

  // Soal 1 (Animal Class) Release 1
  console.log("\nSoal 1 Release 1");

  var sungokong = new Ape("kera sakti");
  console.log("Ape: ", sungokong.name);
  console.log("jumlah kaki", sungokong.legs);
  sungokong.yell(); // "Auooo"

  var kodok = new Frog("buduk");
  console.log("\nFrog: ", kodok.name);
  console.log("jumlah kaki", kodok.legs);
  kodok.jump(); // "hop hop"

  // Soal 2 (Function to Class)
  console.log("\nSoal 2 (Clock)");
  var clock = new Clock({ template: "h:m:s" });
  clock.start();
}

main();
