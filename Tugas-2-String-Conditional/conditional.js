// Tugas Conditional

// Soal 1
console.log("\nSoal 1 If Else");
var nama = "John";
var peran = "Guard";

if (nama == "" && peran == "") {
  console.log("Nama harus diisi!");
} else {
  if (peran == "") {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
  } else {
    var doJob = "";
    if (peran == "Penyhir") {
      doJob = "kamu dapat melihat siapa yang menjadi werewolf!";
    } else if (peran == "Guard") {
      doJob = "kamu akan membantu melindungi temanmu dari serangan werewolf.";
    } else if (peran == "Werewolf") {
      doJob = "Kamu akan memakan mangsa setiap malam!";
    } else {
      doJob = "Kamu tidak dapat melakukan apa-apa";
    }
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo ${peran} ${nama}, ${doJob}`);
  }
}

// Soal 2
console.log("\nSoal 2 Switch Case");
var hari = 21;
var bulan = 1;
var tahun = 1945;

switch (hari >= 1 && hari <= 31) {
  case true:
    hari = hari;
    break;
  default:
    hari = "Kiamat";
    break;
}

switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 11:
    bulan = "Desember";
    break;
  default:
    "Kiamat";
    break;
}

switch (tahun >= 1900 && tahun <= 2200) {
  case true:
    tahun = tahun;
    break;
  default:
    tahun = "Kiamat";
    break;
}

console.log(`${hari} ${bulan} ${tahun}`);
