// Soal Asynchronous

// Soal 1 (Callback Baca Buku)
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let startTime = 10000;
readBooks(startTime, (b = books.shift()), function read(time) {
  books.push(b);
  if (startTime != time) {
    startTime -= b.timeSpent;
    readBooks(time, (b = books.shift()), read);
  }
});
