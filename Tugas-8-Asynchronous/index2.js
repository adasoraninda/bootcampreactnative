// Soal Asynchronous

// Soal 2 (Promise Baca Buku)
var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

let startTime = 10000;
readBooksPromise(startTime, (b = books.shift())).then(function read(time) {
  books.push(b);
  readBooksPromise(time, (b = books.shift()))
    .then(read)
    .catch(() => {
      console.log("Selesai membaca buku");
    });
});
