import React from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";

import colors from "../common/colors";

export default function TextField({ hint, style, secureText = false }) {
  return (
    <View style={[style]}>
      <Text style={styles.hint}>{hint}</Text>
      <TextInput
        style={styles.textField}
        secureTextEntry={secureText}
        selectionColor={colors.secondary}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  hint: {
    fontSize: 16,
    lineHeight: 18.75,
    color: colors.primary,
  },
  textField: {
    marginTop: 4,
    borderColor: colors.primary,
    borderWidth: 1,
    height: 48,
    textDecorationLine: "none",
    paddingHorizontal: 16,
  },
});
