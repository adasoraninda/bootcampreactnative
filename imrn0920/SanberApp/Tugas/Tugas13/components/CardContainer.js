import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";

import CardItem from "./CardItem";

import colors from "../common/colors";

export default function CardContainer({
  text,
  data,
  marginTop,
  isHorizontal = false,
  orientationItem,
}) {
  return (
    <View style={[styles.container, { marginTop: marginTop }]}>
      <Text style={styles.title}>{text}</Text>
      <View style={styles.line} />
      <View style={{ alignItems: "center" }}>
        <FlatList
          horizontal={isHorizontal}
          data={data}
          renderItem={(data) => (
            <CardItem data={data.item} orientationItem={orientationItem} />
          )}
          keyExtractor={(data) => data.id}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 8,
    flex: 1,
    width: "100%",
    padding: 8,
    backgroundColor: colors.grey,
  },
  line: {
    marginTop: 8,
    height: 1,
    width: "100%",
    backgroundColor: colors.darkerGrey,
  },
  title: {
    flex: 1,
    fontSize: 18,
  },
});
