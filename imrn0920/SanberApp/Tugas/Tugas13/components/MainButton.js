import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

import colors from "../common/colors";

export default function MainButton({
  text,
  color = "primary",
  marginTop = 16,
}) {
  return (
    <TouchableOpacity
      style={[
        styles.button,
        { backgroundColor: colors[color], marginTop: marginTop },
      ]}
    >
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 20,
    paddingHorizontal: 36,
    paddingVertical: 6,
    justifyContent: "center",
    alignItems: "center",
    height: 40,
  },
  text: {
    color: colors.white,
    fontSize: 24,
  },
});
