import React from "react";
import Icon from "react-native-vector-icons/MaterialIcons";
import { StyleSheet, Text, View } from "react-native";
import colors from "../common/colors";

export default function CardItem({ data, orientationItem = "column" }) {
  return (
    <View style={[styles.container, { flexDirection: orientationItem }]}>
      <Icon name={data.icon} size={75} color={colors.secondary} />
      <Text style={styles.text}>{data.text}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 8,
    flex: 1,
    alignItems: "center",
  },
  text: {
    padding: 4,
    fontSize: 16,
    fontWeight: "400",
  },
});
