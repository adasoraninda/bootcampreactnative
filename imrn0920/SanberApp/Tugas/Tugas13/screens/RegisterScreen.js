import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  StatusBar,
} from "react-native";

import TextField from "../components/TextField";
import MainButton from "../components/MainButton";

import colors from "../common/colors";

export default function RegisterScreen() {
  return (
    <ScrollView>
      <StatusBar style="auto" />
      <View style={styles.container}>
        <Image style={styles.logo} source={require("../images/logo.png")} />
        <Text style={styles.title}>Register</Text>
        <TextField
          style={[styles.textInput, { marginTop: 36 }]}
          hint={"Username"}
        />
        <TextField style={styles.textInput} hint={"Email"} />
        <TextField
          style={styles.textInput}
          hint={"Password"}
          secureText={true}
        />
        <TextField
          style={styles.textInput}
          hint={"Ulangi Password"}
          secureText={true}
        />
        <MainButton text={"Daftar"} marginTop={40} />
        <Text style={styles.textOr}>atau</Text>
        <MainButton text={"Masuk ?"} color={"secondary"} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 24,
    flex: 1,
    alignItems: "center",
    paddingBottom: 40,
  },
  logo: {
    resizeMode: "stretch",
  },
  title: {
    marginTop: 60,
    fontSize: 24,
    color: colors.primary,
    fontWeight: "400",
  },
  textInput: {
    width: "100%",
    marginTop: 16,
  },
  textOr: {
    color: colors.secondary,
    marginTop: 16,
    fontSize: 24,
  },
});
