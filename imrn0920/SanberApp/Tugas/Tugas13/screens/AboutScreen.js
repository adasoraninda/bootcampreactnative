import React from "react";
import Icon from "react-native-vector-icons/MaterialIcons";
import { ScrollView, StyleSheet, Text, View } from "react-native";

import CardContainer from "../components/CardContainer";

import colors from "../common/colors";

export default function AboutScreen() {
  const dataGit = [
    {
      id: "1",
      icon: "account-circle",
      text: "@adasoraninda",
    },
    {
      id: "2",
      icon: "account-circle",
      text: "@adasoraninda",
    },
  ];

  const dataSocial = [
    {
      id: "1",
      icon: "facebook",
      text: "@adasoraninda",
    },
    {
      id: "2",
      icon: "facebook",
      text: "@adasoraninda",
    },
    {
      id: "3",
      icon: "facebook",
      text: "@adasoraninda",
    },
  ];
  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.title}>Tentang Saya</Text>
        <Icon style={styles.icon} name="account-circle" size={180} />
        <Text style={styles.name}>Ada</Text>
        <Text style={styles.job}>React Native Developer</Text>
        <CardContainer
          text={"Portofolio"}
          marginTop={16}
          data={dataGit}
          isHorizontal={true}
        />
        <CardContainer
          text={"Hubungi saya"}
          marginTop={8}
          data={dataSocial}
          orientationItem={"row"}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 24,
    alignItems: "center",
    flex: 1,
  },
  icon: {
    marginTop: 12,
    borderRadius: 100,
    backgroundColor: colors.darkerGrey,
    color: colors.grey,
  },
  title: {
    fontSize: 36,
    marginTop: 44,
    color: colors.primary,
    fontWeight: "bold",
  },
  name: {
    marginTop: 24,
    color: colors.primary,
    fontWeight: "bold",
    fontSize: 24,
  },
  job: {
    marginTop: 8,
    color: colors.secondary,
    fontWeight: "bold",
    fontSize: 16,
  },
  cardContainer: {
    marginTop: 16,
  },
});
