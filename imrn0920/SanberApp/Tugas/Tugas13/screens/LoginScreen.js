import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  StatusBar,
} from "react-native";

import TextField from "../components/TextField";
import MainButton from "../components/MainButton";

import colors from "../common/colors";

export default function LoginScreen() {
  return (
    <ScrollView>
      <StatusBar style="auto" />
      <View style={styles.container}>
        <Image style={styles.logo} source={require("../images/logo.png")} />
        <Text style={styles.title}>Login</Text>
        <TextField
          style={[styles.textInput, { marginTop: 36 }]}
          hint={"Username / Email"}
        />
        <TextField
          style={styles.textInput}
          hint={"Password"}
          secureText={true}
        />
        <MainButton text={"Masuk"} marginTop={40} color={"secondary"} />
        <Text style={styles.textOr}>atau</Text>
        <MainButton text={"Daftar ?"} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 24,
    flex: 1,
    alignItems: "center",
    paddingBottom: 40,
  },
  logo: {
    resizeMode: "stretch",
  },
  title: {
    marginTop: 60,
    fontSize: 24,
    color: colors.primary,
    fontWeight: "400",
  },
  textInput: {
    width: "100%",
    marginTop: 16,
  },
  textOr: {
    color: colors.secondary,
    marginTop: 16,
    fontSize: 24,
  },
});
