export default {
  primary: "#003366",
  secondary: "#3EC6FF",
  white: "#FFFFFF",
  grey: "#EFEFEF",
  darkerGrey: "#CACACA",
};
