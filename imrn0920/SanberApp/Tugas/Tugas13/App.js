import React from "react";
import { StyleSheet } from "react-native";

import RegisterScreen from "./screens/RegisterScreen";
import LoginScreen from "./screens/LoginScreen";
import AboutScreen from "./screens/AboutScreen";

export default function App() {
  return <AboutScreen />;
}

const styles = StyleSheet.create({});
