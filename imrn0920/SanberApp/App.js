import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

import YouTubeUi from "./Tugas/Tugas12/App";
import ProfileApp from "./Tugas/Tugas13/App";
import TodosApp from "./Tugas/Tugas14/App";
import MyNavigation from "./Tugas/Tugas15/navigation_1";
import Quiz3 from "./Tugas/Quiz3";

export default function App() {
  return <Quiz3 />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
