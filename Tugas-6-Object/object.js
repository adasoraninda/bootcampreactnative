// Soal Object

// Soal 1 (Array to Object)
function arrayToObject(arr) {
  if (!arr.length) console.log("");

  var now = new Date();
  var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
  var key = ["firstName", "lastName", "gender", "age"];
  for (var i = 0; i < arr.length; i++) {
    var people = {};
    for (var j = 0; j < key.length; j++) {
      if (key[j] == "age") {
        if (arr[i][j] <= thisYear) {
          arr[i][j] = thisYear - arr[i][j];
        } else {
          arr[i][j] = "Invalid Birth Year";
        }
      }
      people[key[j]] = arr[i][j];
    }
    console.log(`${i + 1}. ${people.firstName} ${people.lastName} :`, people);
  }
}

// Soal 2 (Shopping Time)
function shoppingTime(memberId = 0, money) {
  if (!memberId) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }
  if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  }

  var account = {
    memberId: memberId,
    money: money,
    listPurchased: [],
    changeMoney: money,
  };

  var product_x = {
    sale: [
      { name: "Baju brand Zoro", price: 500000 },
      { name: "Sepatu brand Stacattu", price: 1500000 },
      { name: "Baju brand H&N", price: 250000 },
      { name: "Sweater brand Uniklooh", price: 175000 },
      { name: "Casing Handphone", price: 50000 },
    ],
  };

  account.listPurchased = product_x.sale
    .filter((product) => product.price <= account.money)
    .sort((a, b) => b.price - a.price)
    .map((product) => {
      account.changeMoney -= product.price;
      return product.name;
    });

  return account;
}

// Soal No 3 (Naik Angkot)
function naikAngkot(listPenumpang) {
  if (!listPenumpang.length) return [];

  list = [];
  rute = ["A", "B", "C", "D", "E", "F"];
  key = ["penumpang", "naikDari", "tujuan", "bayar"];
  for (var i = 0; i < listPenumpang.length; i++) {
    var penumpang = {};
    for (var j = 0; j < key.length; j++) {
      if (key[j] == "bayar") {
        penumpang[key[j]] =
          Math.abs(
            rute.indexOf(penumpang["tujuan"]) -
              rute.indexOf(penumpang["naikDari"])
          ) * 2000;
      } else {
        penumpang[key[j]] = listPenumpang[i][j];
      }
    }
    list.push(penumpang);
  }
  return list;
}

function soal1() {
  console.log("Soal 1 (Array to Object)");
  var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"],
  ];
  arrayToObject(people);
  var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023],
  ];
  arrayToObject(people2);
  arrayToObject([]);
}

function soal2() {
  console.log("\nSoal 2 (Shopping Time)");
  console.log(shoppingTime("1820RzKrnWn08", 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
  console.log(shoppingTime("82Ku8Ma742", 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
}

function soal3() {
  console.log("\nSoal 3 (Naik Angkot)");
  console.log(
    naikAngkot([
      ["Dimitri", "B", "F"],
      ["Icha", "A", "B"],
      ["Alex", "F", "B"],
    ])
  );
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

  console.log(naikAngkot([])); //[]
}

function main() {
  soal1();
  soal2();
  soal3();
}

main();
