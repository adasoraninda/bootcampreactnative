// Tugas Array

// Soal no 1 (Range)
function range(startNum, finishNum) {
  if (startNum == null || finishNum == null) {
    return -1;
  }

  var arr = [];
  if (startNum > finishNum) {
    for (i = startNum; i >= finishNum; i--) {
      arr.push(i);
    }
  } else {
    for (i = startNum; i <= finishNum; i++) {
      arr.push(i);
    }
  }

  return arr;
}

// Soal no 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step = 1) {
  if (startNum == null || finishNum == null) {
    return -1;
  }

  var arr = [];
  if (startNum > finishNum) {
    for (i = startNum; i >= finishNum; i -= step) {
      arr.push(i);
    }
  } else {
    for (i = startNum; i <= finishNum; i += step) {
      arr.push(i);
    }
  }

  return arr;
}

// Soal no 3 (Sum of Range)
function sum(startNum = 0, finishNum = 0, step = 1) {
  var total = 0;
  if (startNum >= finishNum) {
    for (i = startNum; i >= finishNum; i -= step) {
      total += i;
    }
  } else {
    for (i = startNum; i <= finishNum; i += step) {
      total += i;
    }
  }
  return total;
}

// Soal no 4 (Array Multidimensi)
function dataHandling(arr = [[]]) {
  for (i = 0; i < arr.length; i++) {
    for (j = 0; j < arr[i].length; j++) {
      switch (j) {
        case 0:
          console.log(`Nomor ID: ${arr[i][j]}`);
          break;
        case 1:
          console.log(`Nama Lengkap: ${arr[i][j]}`);
          break;
        case 2:
          console.log(`TTL: ${arr[i][j]} ${arr[i][j + 1]} `);
          break;
        case 4:
          console.log(`Hobi: ${arr[i][j]}`);
          break;
        default:
          break;
      }
    }
    console.log();
  }
}

// Soal no 5 (Balik Kata)
function balikKata(text) {
  var str = "";
  for (i = text.length - 1; i >= 0; i--) {
    str += text.charAt(i);
  }
  return str;
}

// Soal no 6 (Metode Array)
function dataHandling2(input) {
  input.splice(
    1,
    input.length,
    "Roman Alamsyah Elsharawy",
    "Provinsi Bandar Lampung",
    "21/05/1989",
    "Pria",
    "SMA Internasional Metro"
  );
  console.log(input);
  console.log(namaBulan(parseInt(input[3].split("/")[1])));
  console.log(
    input[3].split("/").sort(function (value1, value2) {
      return value2 - value1;
    })
  );
  console.log(input[3].split("/").join("-"));
  console.log(input[1].slice(0, 15));
}

function namaBulan(bulan) {
  switch (bulan) {
    case 1:
      return "Januari";
    case 2:
      return "Februari";
    case 3:
      return "Maret";
    case 4:
      return "April";
    case 5:
      return "Mei";
    case 6:
      return "Juni";
    case 7:
      return "Juli";
    case 8:
      return "Agustus";
    case 9:
      return "September";
    case 10:
      return "Oktober";
    case 11:
      return "November";
    case 12:
      return "Desember";
    default:
      return "null";
  }
}

function main() {
  console.log("\nSoal no 1 (Range)");
  console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  console.log(range(1)); // -1
  console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
  console.log(range(54, 50)); // [54, 53, 52, 51, 50]
  //console.log(range(54, 54));
  console.log(range()); // -1

  console.log("\nSoal no 2 (Range with Step)");
  console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
  console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
  console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
  //console.log(rangeWithStep(5, 5, 1));
  console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

  console.log("\nSoal no 3 (Sum of Range)");
  console.log(sum(1, 10)); // 55
  console.log(sum(5, 50, 2)); // 621
  console.log(sum(15, 10)); // 75
  console.log(sum(20, 10, 2)); // 90
  console.log(sum(1)); // 1
  console.log(sum()); // 0

  console.log("\nSoal no 4 (Array Multidimensi)");
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
  ];
  dataHandling(input);

  console.log("\nSoal no 5 (Balik Kata)");
  console.log(balikKata("Kasur Rusak")); // kasuR rusaK
  console.log(balikKata("SanberCode")); // edoCrebnaS
  console.log(balikKata("Haji Ijah")); // hajI ijaH
  console.log(balikKata("racecar")); // racecar
  console.log(balikKata("I am Sanbers")); // srebnaS ma I

  console.log("\nSoal no 6 (Metode Array)");
  var input = [
    "0001",
    "Roman Alamsyah ",
    "Bandar Lampung",
    "21/05/1989",
    "Membaca",
  ];
  dataHandling2(input);
}

main();
