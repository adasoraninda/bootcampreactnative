// Soal Function

// Soal no 1
console.log("Soal 1");
function teriak() {
  return "Halo Sanbers!";
}
console.log(teriak());

console.log("\nSoal 2");
var num1 = 12;
var num2 = 4;
function kalikan(num1 = 0, num2 = 0) {
  return num1 * num2;
}
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log("\nSoal 3");
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
function introduce(name, age, address, hobby) {
  return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address} dan saya punya hobby yaitu ${hobby}!`;
}
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
